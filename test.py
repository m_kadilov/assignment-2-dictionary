import subprocess

inputs = ["book", "university", "subject", "not_found", "toooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo_long"]
outputs = ["LLP", "ITMO", "Programming languages", "", ""]
errors = ["", "", "", "String not found in the dictionary!", "String must be less than 256 symbols!"]

for i in range(len(inputs)):
    process = subprocess.Popen(["./program"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate(input=inputs[i].encode())
    stdout = stdout.decode().strip()
    stderr = stderr.decode().strip()
    print("Test #" + str(i+1) + ": " + inputs[i])

    if stdout == outputs[i] and stderr == errors[i]:
        print("test #" + str(i+1) + ": CORRECT")
    else:
        print("test #" + str(i+1) + ": INCORRECT")

        if stdout != outputs[i]:
            print("Expected in STDOUT: " + outputs[i])
            print("Found in STDOUT: " + stdout)

        if stderr != errors[i]:
            print("Expected in STDERR: " + errors[i])
            print("Found in STDERR: " + stderr)
    print("_______________________________________")
