ASM=nasm
FLAGS=-f elf64
LD=ld
PYTHON=python2

all: program

program: main.o dict.o lib.o
	$(LD) -o $@ $^

%.o: %.asm
	$(ASM) $(FLAGS) -o $@ $<

clean:
	rm *.o

test: all
	$(PYTHON) test.py

.PHONY: all clean test
